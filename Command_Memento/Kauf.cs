﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Memento
{
    [Serializable]
    public class Kauf :Transaction
    {
        public Kauf(string aktie)
        {
            Aktie = aktie;
        }
        public override void Execute()
        {
            IsDone = WifiATX.OrderSystem.Buy(Aktie);
            base.Execute();
        }
        public override string ToString()
        {
            return $"Kauf der Aktie {Aktie} {ExecutionTime()}";
        }
    }
}
