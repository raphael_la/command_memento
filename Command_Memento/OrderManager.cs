﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Memento
{
    [Serializable]
    class OrderManager
    {
        public ConcurrentQueue<ITransaction> Orders { get; set; } = new ConcurrentQueue<ITransaction>();
        
        public bool HatOffeneOrders
        {
            get { return Orders.Any(x => !x.IsDone); }
        }

        public void AddOrder(ITransaction order)
        {
            Orders.Enqueue(order as ITransaction);
        }

        public void ProcessOpenOrders()
        {
            foreach (var order in Orders.Where(x => !x.IsDone))
            {
                order.Execute();
            }
        }
    }
}
