﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Command_Memento
{
    public partial class Form1 : Form
    {
        OrderManager om;
        public Form1()
        {
            InitializeComponent();
            om = DeSerializeOrder() ?? new OrderManager();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            om.AddOrder(new Kauf("OMV"));
            UpdateOrderDisplay();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            om.AddOrder(new Verkauf("OMV"));
            UpdateOrderDisplay();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            om.AddOrder(new Kauf("VOEST"));
            UpdateOrderDisplay();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            om.AddOrder(new Verkauf("VOEST"));
            UpdateOrderDisplay();
        }

        private void UpdateOrderDisplay()
        {
            lstAbgeschlossen.Items.Clear();
            lstOffen.Items.Clear();
            foreach (var item in om.Orders)
            {
                if (item.IsDone)
                    lstAbgeschlossen.Items.Add(item.ToString());
                else
                    lstOffen.Items.Add(item.ToString());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(OrderProcess));
        }

        private void OrderProcess(object state)
        {
            while (true)
            {
                Invoke(new MethodInvoker(UpdateOrderDisplay));
                Thread.Sleep(10000);
                om.ProcessOpenOrders();
            }
        }

        string file = @"C:\temp\orders.bin";

        private void SerializeOrder()
        {
            using(FileStream fs = new FileStream(file, FileMode.OpenOrCreate))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, om);
            }
        }

        private OrderManager DeSerializeOrder()
        {
            if (!File.Exists(file))
                return null;

            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                return bf.Deserialize(fs) as OrderManager;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SerializeOrder();
        }
    }
}
