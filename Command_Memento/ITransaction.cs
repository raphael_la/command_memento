﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Memento
{
    interface ITransaction
    {
        void Execute();
        bool IsDone { get; set; }
        string ExecutionTime();
    }
}
