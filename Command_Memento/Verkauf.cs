﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Memento
{
    [Serializable]
    public class Verkauf :Transaction
    {
        public Verkauf(string aktie)
        {
            Aktie = aktie;
        }
        public override void Execute()
        {
            IsDone = WifiATX.OrderSystem.Sell(Aktie);
            base.Execute();
        }
        public override string ToString()
        {
            return $"Verkauf der Aktie {Aktie} {ExecutionTime()}";
        }
    }
}
