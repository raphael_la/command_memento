﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command_Memento
{
    [Serializable]
    public class Transaction : ITransaction
    {
        string time;

        public virtual void Execute()
        {
            if (IsDone)
                time += " Durchgeführt um " + DateTime.Now.ToLongTimeString();
        }

        public string ExecutionTime()
        {
            if(string.IsNullOrEmpty(time))
                time = $"Plaziert um {DateTime.Now.ToLongTimeString()}";

            return time;
        }

        public bool IsDone { get; set; }

        public string Aktie { get; set; }
    }
}
